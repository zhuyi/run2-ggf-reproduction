#ifndef CORE_CONFIG_READER_H
#define CORE_CONFIG_READER_H

#include "TString.h"

#include "yaml-cpp/yaml.h"

class ConfigReader
{
private:
    TString config_name{"config.yaml"};
};

#endif // CORE_CONFIG_READER_H
