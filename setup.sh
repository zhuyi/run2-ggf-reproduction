#!/bin/bash

echo "setting environment ..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh

export GGF_REPROD_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export GGF_REPROD_INSTALL_DIR="${GGF_REPROD_DIR}/../install"
echo "setting project in ${GGF_REPROD_INSTALL_DIR} ..."
if [ ! -d ${GGF_REPROD_INSTALL_DIR} ]; then
    echo "project run directory doesn't exsit, creating ..."
    mkdir ${GGF_REPROD_INSTALL_DIR}
fi
export PATH="${GGF_REPROD_INSTALL_DIR}/:$PATH"
